//using System.Collections;
//using System.Collections.Generic;
//using System.Reflection;
//using UnityEngine;

///// <summary>
///// 对话事件, 对话结束后调用的方法写在此位置(DialogEvent, SpecialEvent, UsableItemEvent主要为根据触发方式进行分类, 方便管理, 没有非常大的区别)
///// </summary>
//public class DialogEvent : MonoBehaviour
//{
//    public static DialogEvent instance;// 单例

//    private void Awake()
//    {
//        instance = this;
//    }

//    /// <summary>
//    /// 触发事件
//    /// </summary>
//    /// <param name="methodName">方法名</param>
//    public void DialogInvoke(string methodName)
//    {
//        if (methodName == "") return;

//        MethodInfo methodInfo = GetType().GetMethod(methodName);
//        methodInfo.Invoke(this, null);
//    }

//    /// <summary>
//    /// 村庄记录点
//    /// </summary>
//    public void Save_Village()
//    {
//        // save

//        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerAttributes>().respawnPoint = 
//            DialogObjectManager.Instance.GetDialogObject("记录点_村庄").transform.GetChild(0);
//    }

//    /// <summary>
//    /// 打开材料商店
//    /// </summary>
//    public void VillageStore()
//    {
//        InventoryManager.Instance.VillageStoreActiveSwitch();
//    }

//    /// <summary>
//    /// 打开出售界面
//    /// </summary>
//    public void Sell()
//    {
//        InventoryManager.Instance.PackageActiveSwitch(sell : true);
//    }

//    /// <summary>
//    /// 对话后接取任务:找艾克特
//    /// </summary>
//    public void Story_2_Start()
//    {
//        //MainSceneStory.Instance.DriveProcess(1);
//    }

//    /// <summary>
//    /// 对话后接取任务:获得兽肉
//    /// </summary>
//    public void Story_3_GetMeat()
//    {
//        //MainSceneStory.Instance.DriveProcess(2);
//    }

//    /// <summary>
//    /// 狩猎完成对话结束后触发, 扣除玩家的物品成功则更新任务: 查看告示
//    /// </summary>
//    public void Story_6_HuntComplete()
//    {
//        //if (InventoryManager.Instance.ReduceItems(DataManager.instance.itemConfig.FindItemByID(1001), 3))
//            //MainSceneStory.Instance.DriveProcess(3);
//    }

//    /// <summary>
//    /// 在查看告示的对话后触发, 玩家第一次与梅林对话
//    /// </summary>
//    public void Story_7_NoticeMerlin()
//    {
//        //MainSceneStory.Instance.DriveProcess(4);
//    }

//    /// <summary>
//    /// 在与告示牌旁的梅林对话后触发, 播放离开动画
//    /// </summary>
//    public void Story_7_NoticeLeave()
//    {
//        MainSceneStory.Instance.timeline_7_noticeLeave.Play();
//    }

//    /// <summary>
//    /// 打开出售界面
//    /// </summary>
//    public void Story_11_VillageSell()
//    {
//        InventoryManager.Instance.PackageActiveSwitch(sell: true);
//        //MainSceneStory.Instance.DriveProcess(7);
//    }

//    /// <summary>
//    /// 离开村庄
//    /// </summary>
//    public void Story_12_LeaveVillage()
//    {
//        //MainSceneStory.Instance.DriveProcess(8);
//    }
//}
