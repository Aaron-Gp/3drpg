//using System.Collections;
//using System.Collections.Generic;
//using System.Reflection;
//using UnityEngine;

///// <summary>
///// 特殊事件, 特殊事件后(如敌人死亡 物品拾取)调用的方法写在此位置(DialogEvent, SpecialEvent, UsableItemEvent主要为根据触发方式进行分类, 方便管理, 没有非常大的区别)
///// </summary>
//public class SpecialEvent : MonoBehaviour
//{
//    public static SpecialEvent instance;// 单例

//    private int villageSoldier = 8;
//    private int invasionDrops = 6;

//    private void Awake()
//    {
//        instance = this;
//    }

//    /// <summary>
//    /// 触发事件
//    /// </summary>
//    /// <param name="methodName">方法名</param>
//    public void SpecialInvoke(string methodName)
//    {
//        if (methodName == "") return;

//        MethodInfo methodInfo = GetType().GetMethod(methodName);
//        methodInfo.Invoke(this, null);
//    }

//    /// <summary>
//    /// 拾取新手武器后的操作提示
//    /// </summary>
//    public void Story_3_EquipTips()
//    {
//        GameUIManager.Instance.controlTip.ShowTip("‘B’ 打开角色及背包界面\n‘拖动物品’ 装备武器\n‘鼠标左键’ 攻击");
//        GameUIManager.Instance.destinationMark.SetTarget(MainSceneStory.Instance.mark_hunt, 36.0f);

//        DialogObjectManager.Instance.GetDialogObject("打猎_村民甲").SetCommonDialog(MainSceneStory.Instance.dialog_4_A);
//        DialogObjectManager.Instance.GetDialogObject("打猎_村民乙").SetCommonDialog(MainSceneStory.Instance.dialog_4_B);
//        DialogObjectManager.Instance.GetDialogObject("打猎_村民丙").SetCommonDialog(MainSceneStory.Instance.dialog_4_C);
//    }

//    /// <summary>
//    /// 入侵村庄的士兵死亡后触发, 当全部死亡后播放击败敌人的Timeline(Timeline内容: 关闭输入, 显示梅林, 将玩家移动到指定位置并触发对话)
//    /// </summary>
//    public void Story_9_VillageSoldierDied()
//    {
//        villageSoldier--;
//        if (villageSoldier == 0)
//        {
//            Story_9_InvasionEnd();
//        }
//    }

//    /// <summary>
//    /// 播放击败敌人的Timeline(Timeline内容: 关闭输入, 显示梅林, 将玩家移动到指定位置并触发对话)
//    /// </summary>
//    public void Story_9_InvasionEnd()
//    {
//        MainSceneStory.Instance.timeline_9_invasionEnd.Play();
//    }

//    /// <summary>
//    /// 拾取全部战利品后更新提示
//    /// </summary>
//    public void Story_10_PickInvasionDrops()
//    {
//        invasionDrops--;
//        if (invasionDrops == 0)
//        {
//            GameUIManager.Instance.destinationMark.SetTarget(MainSceneStory.Instance.mark_villageTrade);
//        }
//    }
//}
