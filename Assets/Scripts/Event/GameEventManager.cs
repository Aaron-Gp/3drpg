using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameEventManager : MonoBehaviour
{
    private static GameEventManager instance;

    public static GameEventManager Instance
    {
        get
        {
            if (instance == null) instance = GameObject.FindObjectOfType<GameEventManager>();
            return instance;
        }
    }

    public UnityEvent<DialogObject> beforeDialogEvent = new UnityEvent<DialogObject>();
    public UnityEvent<DialogObject> dialogEndEvent = new UnityEvent<DialogObject>();
    public UnityEvent<DialogConfig> dialogConfigEndEvent = new UnityEvent<DialogConfig>();
    public UnityEvent<int> pickUpItemEvent = new UnityEvent<int>();
    public UnityEvent playerRespawnEvent = new UnityEvent();
    public UnityEvent<CharacterAttributes> characterBeforeDeathEvent = new UnityEvent<CharacterAttributes>();
    public UnityEvent<int> playerEquipEvent = new UnityEvent<int>();
    public UnityEvent closePackageEvent = new UnityEvent();
    public UnityEvent storySettingEndEvent = new UnityEvent();
    public UnityEvent sellEvent = new UnityEvent();
}
