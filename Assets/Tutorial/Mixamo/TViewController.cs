using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mixamo
{
    public class TViewController : MonoBehaviour
    {
        public Transform viewPoint;
        public Transform playerView;

        private Vector3 lookRotationEuler;// 朝向的欧拉角

        private void LateUpdate()
        {
            // 相机跟随
            playerView.position = viewPoint.position;

            // 角度旋转
            lookRotationEuler.x += Input.GetAxis("Mouse Y") * 1.0f * -1;
            lookRotationEuler.y += Input.GetAxis("Mouse X") * 2.5f;

            // 越界限制
            lookRotationEuler.x = Mathf.Clamp(lookRotationEuler.x, -60, 80);

            // 视角赋值
            playerView.rotation = Quaternion.Euler(lookRotationEuler);
        }
    }
}
