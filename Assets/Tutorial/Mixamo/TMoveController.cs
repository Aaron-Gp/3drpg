using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mixamo
{
    public class TMoveController : MonoBehaviour
    {
        public Transform playerView;
        public Animator animator;

        private CharacterController characterController;
        private Quaternion targetRotation;

        private void Awake()
        {
            characterController = GetComponent<CharacterController>();
        }

        private void Update()
        {
            // 按键输入向量
            Vector3 inputDir = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));

            // 角色位移向量
            Vector3 moveDir = playerView.TransformDirection(inputDir);
            moveDir.y = 0;
            moveDir.Normalize();

            Vector3 gravity = new Vector3(0, -5.0f, 0);

            // 角色移动
            characterController.Move((moveDir * 3.0f + gravity) * Time.deltaTime);

            // 角色旋转
            if (inputDir.magnitude > 0.1f) targetRotation = Quaternion.LookRotation(moveDir, Vector3.up);
            transform.rotation = targetRotation;

            animator.SetFloat("MoveSpeed", Mathf.Lerp(animator.GetFloat("MoveSpeed"), moveDir.magnitude, 0.05f));
        }
    }
}
